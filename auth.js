const jwt = require("jsonwebtoken");
const secret = "producteCommerceAPI";


module.exports.createAccessToken = (userDetails) => {

	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin,
		isActive: userDetails.isActive
	}

	console.log(data);

	return jwt.sign(data, secret, {});

};


module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization


	if (typeof token === "undefined") {

		return res.send({auth: "Authentication Failed. Please provide the Token."});

	} else {
	
		token = token.slice(7);
		jwt.verify(token, secret, function(err, decodedToken) {

			if (err) {

				return res.send({

					auth: "Request Failed.",
					message: err.message

				})

			} else {
				
				req.user = decodedToken;
				next ();

			}

		})
	}

};


module.exports.verifyAdmin = (req, res, next) => {

	console.log(req.user);
	if (req.user.isAdmin) {

		next ();

	} else {

		return res.send({

			auth: "Request Failed.",
			message: "Action Forbidden! Only an Administrator is allowed."

		})

	}

};


module.exports.verifyActive = (req, res, next) => {

	console.log(req.user);
	if (req.user.isActive) {

		next ();

	} else {

		return res.send({

			auth: "Request Failed.",
			message: "Your Account has been set inactive. Kindly contact your Administrator for details."

		})

	}

};