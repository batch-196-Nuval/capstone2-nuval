const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.registerUser = (req, res) => {

	const hashedPw = bcrypt.hashSync(req.body.password, 10);

	console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


module.exports.loginUser = (req, res) => {

	console.log(req.body);
	
	User.findOne({"email": req.body.email})
	.then(foundUser => {

		if (foundUser === null) {

			return res.send({message: "No User found."})
	
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

			if (isPasswordCorrect) {

				return res.send({accessToken: auth.createAccessToken(foundUser)});

			} else {

				return res.send({message: "Incorrect Password."});
			}

		}

	})

};


module.exports.getUserDetails = (req, res) => {

	console.log(req.user);

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


module.exports.setAdmin = (req, res) => {

	console.log(req.params.userId);
	console.log(req.body);

	let update = {

		isAdmin: true

	}

	User.findByIdAndUpdate(req.params.userId, update, {new: true})
	.then (result => res.send (result))
	.catch (error => res.send(error))

};


module.exports.setUser = (req, res) => {

	console.log(req.params.userId);
	console.log(req.body);

	let update = {

		isAdmin: false

	}

	User.findByIdAndUpdate(req.params.userId, update, {new: true})
	.then (result => res.send (result))
	.catch (error => res.send(error))

};


module.exports.setInactive = (req, res) => {

	console.log(req.params.userId);
	console.log(req.body);

	let update = {

		isActive: false

	}

	User.findByIdAndUpdate(req.params.userId, update, {new: true})
	.then (result => res.send (result))
	.catch (error => res.send(error))

};


module.exports.setActive = (req, res) => {

	console.log(req.params.userId);
	console.log(req.body);

	let update = {

		isActive: true

	}

	User.findByIdAndUpdate(req.params.userId, update, {new: true})
	.then (result => res.send (result))
	.catch (error => res.send(error))

};