const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers");
const auth = require("../auth");

const { verify, verifyAdmin, verifyActive } = auth;



router.post('/', verify, verifyActive, orderControllers.order);


router.get('/get-user-orders', verify, verifyActive, orderControllers.getUserOrders);


router.get('/', verify, verifyAdmin, orderControllers.getAllOrders);


module.exports = router;