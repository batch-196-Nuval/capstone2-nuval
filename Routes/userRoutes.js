const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

const { verify, verifyAdmin, verifyActive } = auth;


router.post("/", userControllers.registerUser);


router.post('/login', userControllers.loginUser);


router.get('/details', verify, verifyActive, userControllers.getUserDetails);


router.put("/set-user-as-admin/:userId", verify, verifyAdmin, userControllers.setAdmin);


router.put("/set-admin-as-user/:userId", verify, verifyAdmin, userControllers.setUser);


router.put("/set-user-inactive/:userId", verify, verifyAdmin, userControllers.setInactive);


router.put("/set-user-active/:userId", verify, verifyAdmin, userControllers.setActive);


module.exports = router;