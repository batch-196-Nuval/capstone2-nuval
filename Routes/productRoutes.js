const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

const { verify, verifyAdmin } = auth;


router.post('/', verify, verifyAdmin, productControllers.createProduct);


router.get('/active', productControllers.getActiveProducts);


router.get('/single-product/:productId', productControllers.getSingleProduct);


router.put("/update-product/:productId", verify, verifyAdmin, productControllers.updateProduct);


router.delete("/archive-product/:productId", verify, verifyAdmin, productControllers.archiveProduct);


router.put("/reactivate-product/:productId", verify, verifyAdmin, productControllers.reactivateProduct);


module.exports = router;